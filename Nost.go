package main

import (
    "log"
    "strconv"
    "strings"
    "time"
    "io/ioutil"
    "os"
    "bufio"
    "regexp"
    tb "gopkg.in/tucnak/telebot.v2"
)

const (
    AdminUsername = "<USERNAME>"
)

var (
    logger   *log.Logger
    reader = bufio.NewReader(os.Stdin)
    Base     []string
    Stats    string
)

func main() {

    var logfile, err = os.OpenFile("nost.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
    if err != nil {
        log.Fatal(err)
    }
    logger = log.New(logfile, "", log.LstdFlags|log.Lshortfile)

    b, err := tb.NewBot(tb.Settings{
        Token:  "<TOKEN>",
        Poller: &tb.LongPoller{Timeout: 10 * time.Second},
    })

    if err != nil {
        logger.Fatal(err)
    }
    LoadBase()
    logger.Println("start")

    // Handlers
    b.Handle(tb.OnText, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnEdited, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnPhoto, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnAudio, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnDocument, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnSticker, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnVideo, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnVoice, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnVideoNote, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnContact, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnLocation, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle(tb.OnVenue, func(m *tb.Message) {
     checkURL(b, m)
    })
    b.Handle("/add", func(m *tb.Message) {
        admins, err := b.AdminsOf(m.Chat)
        if err != nil {
            logger.Println(err)
            b.Send(m.Chat, "Error while checking your rights!")
        }
        for _, admin := range admins {
            if m.Sender.Username == admin.User.Username || m.Sender.Username == AdminUsername {
                for _, blackURL := range Base {
                    if m.Payload == blackURL {
                        logger.Printf("\"%s\" from \"%s\" -> tried + \"%s\" but already exists\n", m.Sender.Username, m.Chat.Title, m.Payload)
                        b.Send(m.Chat, "It already exists!")
                        return
                    }
                }
                logger.Printf("\"%s\" from \"%s\" -> + \"%s\"\n", m.Sender.Username, m.Chat.Title, m.Payload)
                addToBase(m.Payload)
                b.Send(m.Chat, "added!")
                return
            }
        }
        logger.Printf("\"%s\" from \"%s\" -> tried + \"%s\" but not admin\n", m.Sender.Username, m.Chat.Title, m.Payload)
        b.Send(m.Chat, "You are not admin!")

	})

    b.Handle("/size", func(m *tb.Message) {
        logger.Printf("\"%s\" from \"%s\" -> checked blacklist's size\n", m.Sender.Username, m.Chat.Title)
        b.Send(m.Chat, strconv.Itoa(len(Base)) + " hosts!")
    })

    b.Handle("/stats", func(m *tb.Message) {
        logger.Println("\"%s\" from \"%s\" -> checked stats\n", m.Sender.Username, m.Chat.Title)
        b.Send(m.Chat, Stats + " blocked hosts!")
    })

    b.Start()
}

func checkURL(b *tb.Bot, m *tb.Message) {
    message := m.Caption + m.Text

    extractURL := regexp.MustCompile(`[-\w@:%.\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}`)

    for lineNbr, blackURL := range Base {
        if strings.HasPrefix(blackURL, "#") {continue}

	for _, extractedURL := range extractURL.FindAllString(message, -1) {
        if len(blackURL) > 2 && strings.ToLower(extractedURL) == blackURL {
            logger.Printf("\"%s\" from \"%s\" -> \"%s\" deleted (\"%s\" from line \"%d\")\n", m.Sender.Username, m.Chat.Title, message, blackURL, lineNbr)
            stats, err := strconv.Atoi(Stats)
            if err != nil {
                logger.Println(err)
            }
            strstats := strconv.Itoa(stats + 1)
            Stats = strstats
            err = ioutil.WriteFile("stats.txt", []byte(strstats), 0644)
            if err != nil {
                logger.Println(err)
            }
            b.Send(m.Chat, "@"+m.Sender.Username+" Your message has been removed because it included an unauthorized URL.\n\nIn case of false positive, please send your message to "+AdminUsername+" (or an admin) to fix it.")
            b.Delete(m)
	    return
        }
}
    }
}

func addToBase(url string) {

    url = strings.Replace(url, "http://", "", -1)
    url = strings.Replace(url, "https://", "", -1)
    if strings.ContainsAny(url, "/") {
        url = strings.Split(url, "/")[0]
    }

    Base = append(Base, url)

    if _, err := os.Stat("blacklist.txt"); os.IsNotExist(err) {
        _, err = os.Create("blacklist.txt")
        if err != nil {
            logger.Fatal(err)
        }
    }

    f, err := os.OpenFile("blacklist.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
    if err != nil {
        logger.Fatal(err)
    }
    defer f.Close()
    _, err = f.WriteString(url+"\n")
    if err != nil {
        logger.Fatal(err)
    }


}

func LoadBase()  {

    if _, err := os.Stat("blacklist.txt"); os.IsNotExist(err) {
        _, err = os.Create("blacklist.txt")
        if err != nil {
            logger.Fatal(err)
        }
    }

    if _, err := os.Stat("stats.txt"); os.IsNotExist(err) {
        _, err = os.Create("stats.txt")
        if err != nil {
            logger.Fatal(err)
        }
    }
    file, err := os.Open("blacklist.txt")
    if err != nil {
        logger.Fatal(err)
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        Base = append(Base, scanner.Text())
    }
    stats, err := ioutil.ReadFile("stats.txt")
    if err != nil {
        logger.Fatal(err)
    }
    Stats = strings.Replace(strings.Replace(string(stats), "\r", "", -1), "\n", "", -1)


}

