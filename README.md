# Nost \_Telegram\_

A bot to delete unwanted URLs in a chatroom by using a blacklist.

**This bot needs the right to delete other people's messages**

## To edit
In `Nost.go`, replace all the "<…>" with your information (admin username, bot token).

## Explaination
**`updateBlacklist.sh`** downloads a blacklist ([one from StevenBlack](https://github.com/StevenBlack/hosts)) and converts it in the right format (domains only, one line by domain, comments starts by #)

There are some **commands** usable from the chatrooms :

- `/add domain.ext` : adds `domain.ext` to the blacklist
    - for admin and chatrooms admins only
    - these added domains are deleted when you update the blacklist
- `/size` : get the size of the blacklist
- `/stats` : get the number of blocked messages since the start

The **blacklist** is read from a file named `blacklist.txt` in the current directory.

The statistics (`/stats`) are saved in the `stats.txt` file, in the current directory.
