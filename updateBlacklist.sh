#!/bin/sh

# download
wget -O blklst "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn/hosts"

# remove comments
sed -i 's/#.*$//' blklst

# delete all except hosts
sed -i 's/^.* //' blklst

# delete IPs
sed -i 's/^[0-9\.:]*$//' blklst

# delete blank lines
sed -i '/^\s*$/d' blklst

# deduplicate entries
sort -u blklst > blacklist.txt
rm blklst
